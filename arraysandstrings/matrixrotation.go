package arraysandstrings

import "fmt"

func RotateMatrix() {
	const size = 4
	var matrix [size][size]int
	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			matrix[i][j] = i + 1
		}
	}

	fmt.Println("Actual matrix: ")
	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			fmt.Printf("%d\t", matrix[i][j])
		}
		fmt.Println()
	}

	for x := 0; x < size/2; x++ {
		for y := x; y < size-x-1; y++ {
			temp := matrix[x][y]
			matrix[x][y] = matrix[y][size-1-x]
			matrix[y][size-1-x] = matrix[size-1-x][size-1-y]
			matrix[size-1-x][size-1-y] = matrix[size-1-y][x]
			matrix[size-1-y][x] = temp
		}
	}

	fmt.Println("Rotated matrix: ")
	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			fmt.Printf("%d\t", matrix[i][j])
		}
		fmt.Println()
	}

}
