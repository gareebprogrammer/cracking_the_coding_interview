package arraysandstrings

import (
	"strings"
)

//Find if a string  is unique or not //
func IsUnique(str string) bool {
	var tempstr string
	for i := 0; i < len(str); i++ {
		if strings.Contains(tempstr, string(str[i])) {
			return false
		} else {
			tempstr += string(str[i])
		}
	}
	return true
}
