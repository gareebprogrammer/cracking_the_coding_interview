package arraysandstrings

import "strconv"

func StringCompression(input string) string {

	var retstr string

	lenstr := len(input)

	for i := 0; i < lenstr; i++ {

		count := 1

		for i < lenstr-1 && input[i] == input[i+1] {
			count++
			i++
		}
		retstr += string(input[i]) + strconv.Itoa(count)
	}
	if lenstr > len(retstr) {
		return retstr
	} else {
		return input
	}
}
