package arraysandstrings

func Urlify(str string) string {
	var tempstr string
	for i := range str {
		if string(str[i]) == " " {
			tempstr += "%20"
		} else {
			tempstr += string(str[i])
		}
	}
	return tempstr
}
