package arraysandstrings

import (
	"strings"
	"testing"
)

func TestUrlifyOne(t *testing.T) {
	testarena := map[string]string{
		" ":                "%20",
		"This is true":     "This%20is%20true",
		"This is not true": "This%20is%20not%20true",
	}
	for i, j := range testarena {
		temp := Urlify(i)
		if strings.Compare(temp, j) != 0 {
			t.Errorf("Failed in TestUrlifyOne test")
		}
	}
}
