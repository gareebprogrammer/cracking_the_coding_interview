package arraysandstrings

import "testing"

func TestIsUniqueONE(t *testing.T) {
	if IsUnique("abcdefgh") == false {
		t.Errorf("Incorrect answer at testIsUniqueONE")
	}
}

func TestIsUniqueTWO(t *testing.T) {
	if IsUnique("aabbccddeeffgghh") == true {
		t.Errorf("Incorrect answer at testIsUniqueTWO")
	}
}

func TestIsUniqueTHREE(t *testing.T) {
	if IsUnique("crackingthecodeininterview") == true {
		t.Errorf("Incorrect answer at testIsUniqueTHREE")
	}
}

func TestIsUniqueFOUR(t *testing.T) {
	if IsUnique("golang") == true {
		t.Errorf("Incorrect answer at testIsUniqueFOUR")
	}
}

func TestIsUniqueFIVE(t *testing.T) {
	if IsUnique("golangcoding") == true {
		t.Errorf("Incorrect answer at testIsUniqueFIVE")
	}
}
