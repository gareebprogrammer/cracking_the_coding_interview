package arraysandstrings

func StringPermutation(str1, str2 string) bool {
	if len(str1) != len(str2) {
		return false
	}
	counts := make(map[rune]int)
	for _, r := range str1 {
		counts[r]++
	}
	for _, r := range str2 {
		counts[r]--
	}
	for _, val := range counts {
		if val != 0 {
			return false
		}
	}
	return true
}
