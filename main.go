package main

import (
	"fmt"

	"github.com/gareebprogrammer/cracking_the_codeing_interview/arraysandstrings"
)

func main() {
	if arraysandstrings.IsUnique("abcd") == true {
		fmt.Println("All chars are unique")
	} else {
		fmt.Println("All chars are not unique")
	}
}
